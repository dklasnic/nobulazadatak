package com.nobula.zadatak.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.nobula.zadatak.SchedulingService;
import com.nobula.zadatak.dao.Repo;
import com.nobula.zadatak.model.Prime;

@Controller
public class WebController {

	@Autowired
	Repo repo;
	
	@Autowired
	SchedulingService schService;
	
	@GetMapping("/")
	public ModelAndView getHomePage() {
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("index");
		mv.addObject("rate",schService.getRate());
		mv.addObject("storedPrimes", getStoredListOfPrimes());
		return mv;
	}
	
	@PostMapping("/changeMemoryInterval")
	public ModelAndView changeMemoryInterval(@RequestParam Integer interval) {
		schService.setRate(interval);
		
		ModelAndView mv = new ModelAndView();
		mv.addObject("storedPrimes", getStoredListOfPrimes());
		mv.addObject("rate",schService.getRate());
		mv.setViewName("index");
		return mv;
	}
	
	@PostMapping("/clean")
	public ModelAndView cleanMemory() {
		repo.deleteAll();
		ModelAndView mv = new ModelAndView();
		mv.addObject("rate",schService.getRate());
		mv.addObject("storedPrimes", getStoredListOfPrimes());
		mv.setViewName("index");
		return mv;
	}
	
	

	public List<Prime> getStoredListOfPrimes(){
		List<Prime> list = repo.findAll();
		return list;
	}
}
