package com.nobula.zadatak.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.nobula.zadatak.SchedulingService;
import com.nobula.zadatak.dao.Repo;
import com.nobula.zadatak.model.Prime;


@RestController
public class PrimeController {

	
	@Autowired
	Repo repo;
	
	@Autowired
	SchedulingService schService;
	
	@GetMapping("/primes/{number}")
	public String getPrimes(@PathVariable int number) {
	
		String primes;
		
		if (repo.existsBynumberOfPrimes(number))
		 {
			Prime prime = repo.getPrimesByNumberOfPrimes(number);
			return prime.getPrimes();
		 }
		
		primes = calculatePrimes(number);
		Prime prime = new Prime();
		prime.setNumberOfPrimes(number);
		prime.setPrimes(primes);
		repo.save(prime);
		return primes;}
	
	@PostMapping("/primes/clean")
	public void cleanMemory() {
		repo.deleteAll();
	}
	


	
	
	private String calculatePrimes(int number) {
		String primes= "[";
		int nextPrime=2;
		for (int i=0;i<number; i++) {
			while (true)
			{	
				boolean isPrime = true;
				for (int z=2;z<nextPrime;z++) {
					  if (nextPrime % z  == 0)
			                isPrime= false;
					}

				nextPrime++;
				if (isPrime)  {
					primes+= nextPrime-1+",";
					break;
				}
			}
		}
		primes = primes.substring(0, primes.length()-1)+ "]";
		return primes;
	}
}
