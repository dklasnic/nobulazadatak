package com.nobula.zadatak.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nobula.zadatak.model.Prime;

public interface Repo extends JpaRepository<Prime, Integer> {

	Prime getPrimesByNumberOfPrimes(int numberOfPrimes);
	
	boolean existsBynumberOfPrimes(int numberOfPrimes);
	
}
