package com.nobula.zadatak.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.hibernate.annotations.GeneratorType;

@Entity (name = "primes")
public class Prime {
	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	int id;
	int numberOfPrimes;
	 @Column(name="primes", columnDefinition="CLOB NOT NULL") 
	 @Lob 
	String primes;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumberOfPrimes() {
		return numberOfPrimes;
	}
	public void setNumberOfPrimes(int numberOfPrimes) {
		this.numberOfPrimes = numberOfPrimes;
	}
	public String getPrimes() {
		return primes;
	}
	public void setPrimes(String primes) {
		this.primes = primes;
	}
	
	
}
