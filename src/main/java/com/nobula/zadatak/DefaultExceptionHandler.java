package com.nobula.zadatak;


import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException.NotFound;
import org.springframework.web.servlet.ModelAndView;

import com.nobula.zadatak.dao.Repo;
import com.nobula.zadatak.model.Prime;

@ControllerAdvice
public class DefaultExceptionHandler {

	@Autowired
	Repo repo;
	
	@Autowired
	SchedulingService schService;
	
	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	public ModelAndView handleExceptionMethodNotSupported() {
		ModelAndView mv = new ModelAndView();
		mv.addObject("storedPrimes", getStoredListOfPrimes());
		mv.addObject("rate",schService.getRate());
		mv.setViewName("index");
		return mv;
		
    
	}
	
	public List<Prime> getStoredListOfPrimes(){
		List<Prime> list = repo.findAll();
		return list;
	}
}
