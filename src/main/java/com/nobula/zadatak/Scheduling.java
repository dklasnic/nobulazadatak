package com.nobula.zadatak;


import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import com.nobula.zadatak.dao.Repo;

@Configuration
public class Scheduling implements SchedulingConfigurer  {
	
	
	@Autowired
	SchedulingService schedulingService;
	
	@Autowired
	Repo repo;
	
	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
	    taskRegistrar.addTriggerTask(new Runnable() {

			@Override
			public void run() {
				repo.deleteAll();
			}
	} , new Trigger() {

		@Override
		public Date nextExecutionTime(TriggerContext triggerContext) {
			 Calendar nextExecutionTime = new GregorianCalendar();
             Date lastActualExecutionTime = triggerContext.lastActualExecutionTime();
             nextExecutionTime.setTime(lastActualExecutionTime != null ? lastActualExecutionTime : new Date());
             nextExecutionTime.add(Calendar.MILLISECOND, getNewExecutionTime());
			return nextExecutionTime.getTime();
		}});
	
	}
    private int getNewExecutionTime() {
        
        return  schedulingService.getRate();
    }
}
